// [SECTION] DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.gw82m.mongodb.net/Captsone-2?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedToPology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))