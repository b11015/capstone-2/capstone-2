const Product = require('../models/Product');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
const { reset } = require('nodemon');

// CREATE PRODUCT ADMIN ONLY

module.exports.addProduct = (req, res) => {
    console.log(req.body)

    Product.findOne({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }).then(result => {
        console.log(result)

        if (result !== null && result.name == req.body.name, req.body.description, req.body.price) {
            return res.send('Product already exists')
        } else {
            let newProduct = new Product({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price
            })

            newProduct.save()
                .then(result => res.send(result))
                .catch(err => res.send(err))
        }
    })
};

// module.exports.addProduct = (req, res) => {
//     console.log(req.body)
//     let newProduct = new Product({
//         name: req.body.name,
//         description: req.body.description,
//         price: req.body.price
//     })

//     newProduct.save()
//         .then(result => res.send(result))
//         .catch(err => res.send(err))
// };

// GET ALL PRODUCTS
module.exports.getAllProducts = (req, res) => {
    Product.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// RETRIEVE ALL PRODUCTS
module.exports.retrieveAllActiveProducts = (req, res) => {
    Product.find({ isActive: true })
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// RETRIEVE SINGLE PRODUCT
module.exports.retrieveSingleProduct = (req, res) => {
    console.log(req.params);

    Product.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// UPDATE PRODUCT ADMIN ONLY
module.exports.updatedProduct = (req, res) => {
    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(updatedProduct => res.send(updatedProduct))
        .catch(err => res.send(err))
};

// ARCHIVE PRODUCT ADMIN ONLY
module.exports.archiveProduct = (req, res) => {
    let updates = {
        isActive: false
    }

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(archiveProduct => res.send(archiveProduct))
        .catch(err => res.send(err))
};

// FIND PRODUCT BY NAME
module.exports.findProductsByName = (req, res) => {
    console.log(req.body)

    Product.find({ name: { $regex: req.body.name, $options: '$i' } })
        .then(result => {
            if (result.length === 0) {
                return res.send('No product found')
            } else {
                return res.send(result)
            }
        })
};