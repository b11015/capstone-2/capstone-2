const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
const { reset } = require('nodemon');
const Product = require('../models/Product');

// REGISTER USER
module.exports.registerUser = (req, res) => {
    console.log(req.body);

    User.findOne({ email: req.body.email }).then(result => {
        console.log(result);

        if (result !== null && result.email == req.body.email) {
            return res.send('Account is already registered')
        } else {
            const hashedPW = bcrypt.hashSync(req.body.password, 10)
            let newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: hashedPW,
            })

            newUser.save()
                .then(result => res.send(result))
                .catch(err => res.send(err))
        }
    })
}


// module.exports.registerUser = (req, res) => {
//     console.log(req.body)


//     const hashedPW = bcrypt.hashSync(req.body.password, 10);
//     let newUser = new User({
//         firstName: req.body.firstName,
//         lastName: req.body.lastName,
//         email: req.body.email,
//         password: hashedPW,

//     });

//     newUser.save()
//         .then(result => res.send(result))
//         .catch(err => res.send(err))
// };

module.exports.checkEmailExists = (req, res) => {
    console.log(req.body.email)
    User.find({ email: req.body.email })
        .then(result => {
            if (result.length === 0) {
                return res.send('Email is available')
            } else {
                res.send('Email is already registered')
            }
        })
        .catch((err) => res.send(err))
}


// GET ALL USERS
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// LOG IN USER / USER AUTHENTICATION
module.exports.loginUser = (req, res) => {
    console.log(req.body);

    User.findOne({ email: req.body.email })
        .then(foundUser => {
            if (foundUser === null) {
                return req.send("No user found in the database")
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
                console.log(isPasswordCorrect);

                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(foundUser) })
                } else {
                    return res.send("Incorrect password, please try again")
                }
            }
        })
        .catch(err => res.send(err));
};

// MAKE ADMIN
module.exports.makeAdmin = (req, res) => {
    console.log(req.user.id);
    console.log(req.params.id);

    let updates = {

        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(makeAdmin => res.send(makeAdmin))
        .catch(err => res.send(err));
};

// SET USER ADMIN
module.exports.setUserAdmin = (req, res) => {
    console.log(req.user.id);
    console.log(req.params.id);

    let updates = {

        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(setUserAdmin => res.send(setUserAdmin))
        .catch(err => res.send(err));
};

// CHECK EMAIL IF EXISTS
module.exports.checkEmailExists = (req, res) => {
    console.log(req.body.email)
    User.find({ email: req.body.email })
        .then(result => {
            if (result.length === 0) {
                return res.send('Email is available')
            } else {
                res.send('Email is already registered')
            }
        })
        .catch((err) => res.send(err))
};

// GET SINGLE USER DETAILS
module.exports.getSingleDetails = (req, res) => {
    console.log(req.body)
    console.log(req.user.id)

    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// ADD PRODUCT USER ADMIN ONLY
module.exports.order = async(req, res) => {
    console.log(req.user.id)
    console.log(req.body.productId)

    if (req.user.isAdmin) {
        return res.send("Action Forbidden")
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        console.log(user)

        let newProduct = {
            productId: req.body.productId,
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
        }

        user.products.push(newProduct);
        user.totalAmount.push(newProduct.price);

        console.log(user.products);
        let sum = user.totalAmount.reduce((prev, current) => prev + current);

        User.findByIdAndUpdate(
            req.user.id, { Total: sum }, { new: true }
        )

        .then((user) => user)
            .catch((err) => err.message);

        return user.save()
            .then(user => true)
            .catch(err => err.message)
    })

    if (isUserUpdated !== true) {
        return res.send({ message: isUserUpdated })
    }

    let isProductUpdated = await Product.findById(req.body.productId).then(product => {
        console.log(product)

        let buyer = {
            buyerId: req.user.id
        }

        product.buyers.push(buyer)


        return product.save()
            .then(product => true)
            .catch(err => err.message)
    })

    if (isProductUpdated !== true) {
        return res.send({ message: isProductUpdated })
    }
    if (isUserUpdated && isProductUpdated) {
        return res.send({ message: "Product checkout successful" })
    }
};

// RETRIEVE AUTHENTICATED USER'S ORDERS
module.exports.getProducts = (req, res) => {
    User.findById(req.user.id)
        .then(result => res.send(result.products))
        .catch(err => res.send(err))
};

module.exports.retrieveAllOrders = (req, res) => {
    let arr = [];
    Product.find({})
        .then((result) => {
            result.forEach((item) => {
                console.log(item);
                item.buyers.length !== 0 &&
                    arr.push({
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        buyers: item.buyers
                    });
            });
            res.send({ arr });
        })
        .catch((err) => res.send(err));
};