const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

const { verify, verifyAdmin } = auth;

console.log(userControllers);

// REGISTER USER
router.post('/', userControllers.registerUser);

// MAKE ADMIN
router.put('/makeAdmin/:id', verify, userControllers.makeAdmin);

// SET USER ADMIN
router.put('/setUserAdmin/:id', verify, verifyAdmin, userControllers.setUserAdmin);

// LOG IN USER / USER AUTHENTICATION
router.post('/login', userControllers.loginUser);

// GET ALL USERS
router.get('/', userControllers.getAllUsers)

// CHECK EMAIL IF EXISTS
router.post('/checkEmailExists', userControllers.checkEmailExists);

// GET SINGLE USER DETAILS
router.get('/getSingleDetails', verify, userControllers.getSingleDetails);

// ADD PRODUCT USER ADMIN ONLY
router.post('/order', verify, userControllers.order);

// RETRIEVE AUTHENTICATED USER'S ORDERS
router.get('/getProducts', verify, userControllers.getProducts);

// RETRIEVE ALL ORDERS
router.get("/retrieveAllOrders", verify, verifyAdmin, userControllers.retrieveAllOrders);

module.exports = router;