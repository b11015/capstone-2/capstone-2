const express = require('express');
const router = express.Router();

const productControllers = require('../controllers/productControllers');

console.log(productControllers);

const auth = require('../auth');

const { verify, verifyAdmin } = auth;

// CREATE PRODUCT ADMIN ONLY
router.post('/', verify, verifyAdmin, productControllers.addProduct);

// GET ALL PRODUCTS
router.get('/getAllProducts', productControllers.getAllProducts);

// RETRIEVE ALL PRODUCTS
router.get('/retrieveAllActiveProducts', productControllers.retrieveAllActiveProducts);

// RETRIEVE SINGLE PRODUCT
router.get('/retrieveSingleProduct/:id', productControllers.retrieveSingleProduct);

// UPDATE PRODUCT ADMIN ONLY
router.put('/:id', verify, verifyAdmin, productControllers.updatedProduct)

// ARCHIVE PRODUCT ADMIN ONLY
router.put('/archive/:id', verify, verifyAdmin, productControllers.archiveProduct);

// FIND PRODUCT BY NAME
router.post('/findProductsByName', productControllers.findProductsByName);

module.exports = router;