const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Firstname is required"]
    },
    lastName: {
        type: String,
        required: [true, "Lastname is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    products: [{
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        },
        purchasedOn: {
            type: Date,
            default: new Date()
        },
        name: {
            type: String,
            required: [true, "Product name is required"]
        },
        description: {
            type: String,
            required: [true, "Product description is required"]
        },
        price: {
            type: String,
            required: [true, "Product price is required"]
        },

    }],
    totalAmount: [{
        type: Number,
        default: 0
    }],
    Total: Number

});

module.exports = mongoose.model("User", userSchema);